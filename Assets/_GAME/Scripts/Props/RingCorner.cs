using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class RingCorner : MonoBehaviour//, IGameEventListener
{
    [SerializeField] List<Vector3> PositionsAvailable;
    public GameEvent OnHeroSelected;
    public GameEvent OnHeroUnSelected;
    [SerializeField]  LimitedHeroListScriptable HerosSelected;
    [SerializeField]  LimitedHeroListScriptable HerosAvailable;
    List<Vector3> PositionsTaken;
    Vector3 TempV3;
    bool ready;
    bool isTeamComplete
    {
        get
        {
            return HerosSelected.isFull;
        }
    }
    private void Start()
    {
        init();
    }
    public void init()
    {

        OnHeroSelected.RegisterAction(OnContainerAdd);
        OnHeroUnSelected.RegisterAction(OnContainerRemoved);

    }
    public Vector3 GetFreePosition()
    {
        if (PositionsAvailable.Count > 0)
        {
            TempV3 = PositionsAvailable[0];
            PositionsAvailable.RemoveAt(0);
            if (PositionsTaken == null)
                PositionsTaken = new List<Vector3>();
            PositionsTaken.Add(TempV3);
        }
        else
            TempV3 = GameParameters.s_Vector3_NoWhere;

        return TempV3;

    }

    public void ReturnPosition(Vector3 Position)
    {
        if(PositionsTaken==null)
            PositionsTaken=new List<Vector3>();
        if (PositionsTaken.Contains(Position))
        {
            PositionsTaken.Remove(Position);
            PositionsAvailable.Insert(0, Position);
        }
    }

    #region Gizmos
    private void OnDrawGizmos()
    {
        DrawGizmoSpheres(Color.red, PositionsTaken);
        DrawGizmoSpheres(Color.green, PositionsAvailable);
    }
    public void DrawGizmoSpheres(Color color, ICollection Positions)
    {
        Gizmos.color = color;
        if (PositionsTaken != null)
            foreach (Vector3 position in Positions)
                Gizmos.DrawSphere(transform.position + transform.rotation* position, GameParameters.s_GizmoSphereRadius);
    }
    #endregion
  /*  public void RaiseEvent(EventArgs Args)
    {

        GenericEventArg<HeroData> genericEventArg = Args as GenericEventArg<HeroData>;

        if (genericEventArg != null && GameParameters.IsState(GameState.Selection))
        {
            switch (genericEventArg.EventType)
            {
                case GenericEventType.OnGameStateChanged:
                    break;
                case GenericEventType.OnContainerAdd:
                    PlaceCharacter(genericEventArg.Data);
                    break;
                case GenericEventType.OnContainerRemove:
                 
                    ReturnPosition(Quaternion.Inverse(transform.rotation)* (genericEventArg.Data.getCurrentGameObject().transform.position - transform.position));
                    genericEventArg.Data.getCurrentGameObject().transform.position = GameParameters.V3_NOWHERE;
                    break;
                case GenericEventType.OnContainerFillStateChanged:
                    break;
                default:
                    break;
            }


        }
    }*/

    public void OnContainerAdd(EventArgs Args)
    {
        if (GameParameters.IsState(GameState.Selection))
        {
            GenericEventArg<HeroData> genericEventArg = Args as GenericEventArg<HeroData>;
            PlaceCharacter(genericEventArg.Data);
        }
    }
    public void OnContainerRemoved(EventArgs Args)
    {
        GenericEventArg<HeroData> genericEventArg = Args as GenericEventArg<HeroData>;
        ReturnPosition(Quaternion.Inverse(transform.rotation) * (genericEventArg.Data.GetCurrentGameObject().transform.position - transform.position));
        if (GameParameters.IsState(GameState.Selection))
        {
            genericEventArg.Data.GetCurrentGameObject().transform.position = GameParameters.s_Vector3_NoWhere;
        }
    }
    public void PlaceCharacter(HeroData data)
    {

        GameObject TempGameObject = data.GetCurrentGameObject();
        TempGameObject.transform.position = transform.position + transform.rotation* GetFreePosition();
        TempGameObject.transform.rotation = transform.rotation;

    }
    /*  public void placeSelectedCharacters()
      {
          for (int i = 0; i < GameParameters.getSelectedHeros().Count; i++)
          {
              HeroData Temp = GameParameters.getSelectedHeros()[i];
              GameObject TempGameObject = Temp.getCurrentGameObject();
              TempGameObject.transform.position = GetFreePosition();
          }
      }
        public void ClearCharacterPositions()
      {
          for (int i = 0; i < GameParameters.getAllHeros().Count; i++)
          {
              HeroData Temp = GameParameters.getAllHeros()[i];
              GameObject TempGameObject = Temp.getCurrentGameObject();
              TempGameObject.transform.position = GameParameters.V3_NOWHERE;
          }

          while (PositionsTaken != null && PositionsTaken.Count > 0)
          {
              PositionsAvailable.Insert(0, PositionsTaken[PositionsTaken.Count - 1]);
              PositionsTaken.RemoveAt(PositionsTaken.Count - 1);
          }
      }

     
     */

    private void OnDestroy()
    {
        OnHeroSelected.UnregisterAction(OnContainerAdd);
        OnHeroUnSelected.UnregisterAction(OnContainerRemoved);
    }

}
