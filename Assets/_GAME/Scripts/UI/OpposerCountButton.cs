using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpposerCountButton : CountButton
{
    public override string GetButtonText()
    {
        return "Opposer : " + GameParameters.GetAICharacterCount();
    }
}
