using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InfoPopupTrigger<T> : MonoBehaviour where T : Component
{
    public float delay = 3;
    public abstract string GetPopupText();
    public virtual void OnMouseDown() => InfoPopup.ClickStarted(GetPopupText(), delay);

    public virtual void OnMouseExit() => InfoPopup.ClickCancelled();

    public virtual void OnMouseUp()
    {
        if (InfoPopup.Instance.gameObject.activeSelf)
            InfoPopup.stop();
        else InfoPopup.ClickCancelled();
    }
}
