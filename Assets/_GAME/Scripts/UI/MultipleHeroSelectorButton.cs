using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MultipleHeroSelectorButton : MultipleSelectionButton<HeroData>,IPointerDownHandler,IPointerExitHandler
{

    
    [SerializeField] Image Icon;
    [SerializeField] Image outline;
    [SerializeField] Sprite Empty;
    [SerializeField] Color NormalOutlineColor;
    [SerializeField] Color SelectedOutlineColor;
    private void OnEnable()
    {
        Icon.sprite = Item != null ? Item.GetIcon() : Empty;
        outline.color = NormalOutlineColor;
        
    }

    public override void Select()
    {
        if (Selector.CanSelect(Index))
            base.Select();
    }
    public override void UnSelect()
    {
        if (isSelected)
            base.UnSelect();
    }
    public override void OnSelect()
    {
        if (isSelected)
            outline.color = SelectedOutlineColor;
    }

    public override void OnUnSelect()
    {
        if (!isSelected)
            outline.color = NormalOutlineColor;
        
    }

    public override ISelector<HeroData> GetSelector()
    {
        return GetComponentInParent<ISelector<HeroData>>();
    }
    public void OnPointerDown(PointerEventData eventData) {
        if(GetSelector()!=null&& GetSelector().GetItem(Index)!=null)
        InfoPopup.ClickStarted(GetSelector().GetItem(Index).ToString(), 3); 
    }
    public void OnPointerExit(PointerEventData eventData) => InfoPopup.ClickCancelled();

    public override void OnPointerUp(PointerEventData eventData)
    {
        if (InfoPopup.isUp())
        {
           InfoPopup.stop();
        }
        else
        {
            InfoPopup.ClickCancelled();

            if (!isSelected)
                Select();
            else
                UnSelect();
        }
    }

    private void OnDisable()
    {
        outline.color = NormalOutlineColor;
    }

}
