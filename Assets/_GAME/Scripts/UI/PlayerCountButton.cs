using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCountButton : CountButton
{
    public override string GetButtonText()
    {
        return "Player : " + GameParameters.GetPlayerCharacterCount();
    }
}
