using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredefinedChangeStateAction : MonoBehaviour
{
   [SerializeField] GameState toState;
    public void ChangeState()
    { 
    GameParameters.SetState(toState);
    }
}
