using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPopup : MonoBehaviour
{
   public static InfoPopup Instance;
   public static GameObject PopCoroutineHolder;
    public GameObject Panel;
    public Text InfoText;
    float HoldDuration;
    bool Holding;
    
    private void Awake()
    {
        Instance = this;
        Panel.SetActive(false);
    }
    public static void ClickStarted(string text, float duration)
    {
       Instance.InfoText.text = text;
       Instance.HoldDuration = duration;
       Instance.Holding = true;
       Instance.StartCoroutine(Instance.CheckHold(duration));
    }
    public static void ClickCancelled()
    {
        if (Instance == null)
            return;
        Instance.Holding = false;
        Instance.StopAllCoroutines();
    }
    public static void pop(Vector3 position)
    {
        Instance.Panel.transform.position = position;
        Instance.Panel.SetActive(true);
    }
    public static void stop()
    {
        Instance.Panel.SetActive(false);
    }

    public static bool isUp()
    {
        return Instance.Panel.activeSelf;
    }
    IEnumerator CheckHold(float duration)
    {
        yield return new WaitForSeconds(duration);
        if (Holding)
        {
                InfoPopup.pop(Input.mousePosition);
        }
    }
}
