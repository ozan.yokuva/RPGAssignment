using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyingText : MonoBehaviour
{
    static FlyingText Instance;
    public CanvasGroup CanvasGroupFlyingText;
    public Text _Text;
    Vector3 FlightPos;
    private void Awake()
    {
        Instance = this;
        CanvasGroupFlyingText.alpha = 0;
    }
   public static void fly(string text,Vector3 pos,float duration, float speed,float wiggleWidth)
    {
        Instance.CanvasGroupFlyingText.alpha = 1;
        Instance.StartCoroutine(Instance.Flying(text, pos,duration,speed,wiggleWidth));

    }

    IEnumerator Flying(string text,Vector3 pos, float duration, float speed, float wiggleWidth)
    {
        transform.position = FlightPos = pos;
        Instance._Text.text = text;
     
        while (Instance.CanvasGroupFlyingText.alpha > 0)
        {
            Instance.CanvasGroupFlyingText.alpha-= Time.deltaTime/ duration;
            FlightPos.y += speed*100*Time.deltaTime;
            FlightPos.x += Mathf.Sin(Time.time) * wiggleWidth;
            transform.position = FlightPos;
            yield return new WaitForEndOfFrame();
        }
    }
}
