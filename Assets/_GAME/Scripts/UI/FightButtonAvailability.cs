using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FightButtonAvailability : MonoBehaviour,IGameEventListener
{
    [SerializeField]GameEvent FillStateChanged;
    [SerializeField]LimitedListScriptable<HeroData>[] SelectionScriptables;
    Button button;
    public void RaiseEvent(EventArgs Args)
    {
        button.interactable = true;
        foreach (var sel in SelectionScriptables)
            button.interactable &= sel.isFull;


    }

    void Start()
    {
        button = GetComponent<Button>();
        FillStateChanged.Register(this);
        button.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnDestroy()
    {
        FillStateChanged.Unregister(this);
    }
}
