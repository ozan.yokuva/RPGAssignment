using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract  class CountButton : MonoBehaviour
{
    [SerializeField] Text ButtonText;

    void Start()
    {
        UpdateCall();
    }

    abstract public string GetButtonText();
    // Update is called once per frame
    void UpdateCall()
    {
        ButtonText.text = GetButtonText();
    }
}
