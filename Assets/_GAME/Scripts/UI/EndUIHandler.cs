using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndUIHandler : MonoBehaviour
{

    [SerializeField] GameEvent OnGameStateChanged;
    [SerializeField] GameObject NewCharUI;
    [SerializeField] Image NewCharacterImage;
    [SerializeField] Text TitleText;
    [SerializeField] Text SummaryText;

    private void Awake()
    {
        OnGameStateChanged.RegisterAction(OnGameStateChange);
    }
    //Handle WinCase!!!
    public void OnGameStateChange(EventArgs eventArgs)
    {
        switch (GameParameters.GetState())
        {
            case GameState.Win:
                updateTitle(true);
                updateSummary();
                updateBattleEx();
                break;
            case GameState.Loose:
                updateTitle(false);
                updateSummary();
                updateBattleEx();
                break;
          
        }


    }

    void updateTitle(bool win)
    {
        if (win)
        {
            TitleText.text = "!!YOU WIN";
            TitleText.color = Color.green;
        }
        else
        {
            TitleText.text = "!!YOU LOOSE";
            TitleText.color = Color.red;

        }
    }
    void updateSummary()
    {
        String Summary = "Summary :";

        foreach (HeroData hero in GameParameters.GetPlayerSelectedHeros())
        {

            Summary = Summary + "\n" + hero.GetName() + " Ex: +1 ";
            if (hero.AddExperience(1))
                Summary = Summary + "LevelUp(" + hero.GetLevel() + ")";
        }
        SummaryText.text = Summary;
    }
    void updateBattleEx()
    {
        HeroData newHero = GameParameters.AddBattleExperience(1);
        if (newHero != null)
        {
            NewCharUI.SetActive(true);
            NewCharacterImage.sprite = newHero.GetIcon();
        }
        else
            NewCharUI.SetActive(false);

    }

    public void CleanUpForNewGame()
    {
        foreach (HeroData hero in GameParameters.GetAllHeros())
        {
            GameObject heroGameObject = hero.GetCurrentGameObject();
            GameParameters.GetPlayerSelectedHeros().Remove(hero);
            GameParameters.GetOpposerSelectedHeros().Remove(hero);
            if (heroGameObject!=null)
                Destroy(heroGameObject);
            GameParameters.SetState(GameState.Selection);
        }
        GameParameters.Reset();
    }
    private void OnDestroy()
    {
        OnGameStateChanged.UnregisterAction(OnGameStateChange);
    }

}

