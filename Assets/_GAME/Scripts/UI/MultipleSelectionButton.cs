using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class MultipleSelectionButton<T> :MonoBehaviour, IPointerUpHandler where T : class
{

    public int Index=0;
    public ISelector<T> Selector { get => GetSelector(); }
    public bool isSelected
    {
        get { 
        return Selector.IsSelected(Index);
        }
    }
    public T Item
    {
        get {
           
            return Selector.GetItem(Index);
        }
    }
    public virtual void OnPointerUp(PointerEventData eventData)
    {
    
        if (!isSelected)
            Select();
        else
            UnSelect();
    }

    public virtual void Select()
    {
        Selector.Select(Index);
        OnSelect();
     
    }
    public virtual void UnSelect()
    {
        Selector.UnSelect(Index);
        OnUnSelect();


    }

    public abstract ISelector<T> GetSelector();
    public abstract void OnSelect();
    public abstract void OnUnSelect();
}
