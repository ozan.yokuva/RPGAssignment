﻿using System;
using UnityEngine;

    public class Billboard : MonoBehaviour
    {
        public Camera Cam;
        public Camera _Cam
        {
            get
            {
                if (Cam == null)
                    Cam = Camera.main;

                return Cam;
            }
        }

 

        private void LateUpdate()
        {

            if (_Cam != null)
                transform.LookAt(transform.position + Cam.transform.forward);
        }



    // Update is called once per frame

}
