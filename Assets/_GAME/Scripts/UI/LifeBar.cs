using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour
{
    public Image LifeImage;
    public Canvas visuals;
    ILife lifeHandler;
    
    
    void Start()
    {
        lifeHandler = GetComponentInParent<ILife>();
        lifeHandler.RegisterOnLifeChange(OnLifeChanged);
        visuals.enabled = true;
    }
    public void OnLifeChanged()
    {
        LifeImage.fillAmount = lifeHandler.GetHealtPercent();
        if (LifeImage.fillAmount == 0)
            visuals.enabled = false;
    }
    
}
