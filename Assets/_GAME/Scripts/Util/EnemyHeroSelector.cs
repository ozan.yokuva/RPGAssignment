using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyHeroSelector : LimitedListScriptableSelector<HeroData>
{
    [SerializeField] GameEvent OnSelectionAdded;

    void Start()
    {
        OnSelectionAdded.RegisterAction(SelectHeros);    
    }

    // Update is called once per frame

    public void SelectHeros(EventArgs args)
    {
     
        while (!(SelectedOptions ).isFull)
            selectRandom();
    }

    public void selectRandom()
    { 
     List<HeroData> searchResult =  Options.TheList.FindAll(x => !SelectedOptions.Contains(x));
        Select(Options.IndexOf(searchResult[Random.Range(0, searchResult.Count)]));

    }


}
