
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(StateBasedObjectEnabler), true), CanEditMultipleObjects]
public class StateBasedObjectEnablerEditor : UnityEditor.Editor
{ 
   
    
    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();
        StateBasedObjectEnabler SBOE = (StateBasedObjectEnabler)target;
        EditorUtility.SetDirty(SBOE);
        int flag = EditorGUILayout.MaskField("Working States", SBOE.StateFlags, GameState.GetNames(typeof(GameState)));
        if(flag!= SBOE.StateFlags)
            SBOE.StateFlags = flag;
       
    }
}




