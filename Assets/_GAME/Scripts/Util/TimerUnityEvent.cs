using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimerUnityEvent : MonoBehaviour
{
    [SerializeField] UnityEvent PredefinedEvent;
    [SerializeField] float Duration;
    [SerializeField] bool StartOnEnable;
    public void StartTimer(float duration)
    { 
        Duration = duration;
        StartCoroutine(TimerTick(duration));
    }
    IEnumerator TimerTick(float Duration)
    {
        yield return new WaitForSeconds(Duration);
        PredefinedEvent?.Invoke();
    }

    private void OnEnable()
    {
        if(StartOnEnable)
        StartTimer(Duration);
    }
    private void OnDisable()
    {
        StopAllCoroutines();
    }

}
