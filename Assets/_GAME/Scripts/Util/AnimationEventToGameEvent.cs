using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEventToGameEvent : MonoBehaviour
{
    [SerializeField] GameEvent TriggerEvent;

    public void Trigger()
    {
        TriggerEvent?.Invoke();
    }

}
