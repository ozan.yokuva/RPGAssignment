using System;
using UnityEngine;

public class StateBasedObjectEnabler : MonoBehaviour
{
    [SerializeField] GameEvent OnGameStateChanged;
     [HideInInspector]public int StateFlags;
    [SerializeField] bool DisableOnStart;
    void Start()
    {
        OnGameStateChanged.RegisterAction(OnGameStateChange);
        if(DisableOnStart)
            gameObject.SetActive(false);
    }


    public void OnGameStateChange(EventArgs args)
    {
        gameObject.SetActive((StateFlags == (StateFlags | (1 << (int)GameParameters.GetState()))));

    }

    private void OnDestroy()
    {
        OnGameStateChanged.UnregisterAction(OnGameStateChange);
    }
}

/*
  mono.StateFlags = EditorGUILayout.MaskField("Working States", mono.StateFlags, Enum.GetNames(typeof(GameState)));
        if (mono._StateVectors==null || mono._StateVectors.Length != Enum.GetValues(typeof(GameState)).Length)
            mono._StateVectors = new Vector3[Enum.GetValues(typeof(GameState)).Length];
if(StateFlags == (StateFlags | (1 << (int)_Parameters.State)))

 */
