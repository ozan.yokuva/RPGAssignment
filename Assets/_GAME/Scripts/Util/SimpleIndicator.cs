using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleIndicator : MonoBehaviour
{
    public GameEvent OnGameStateChange;
    public Vector3 TempV3;
    GameObject follow;

    void Awake() => OnGameStateChange.RegisterAction(OnGameStateChanged);

    public void OnGameStateChanged(EventArgs Args)
    {
        switch (GameParameters.GetState())
        {
    
            case GameState.PlayerSelectTarget:
            case GameState.OpposerSelectTarget:
                follow = BattleData.Instance.Attacker.GetCurrentGameObject();
                break;
         
        }
    }
    private void OnDestroy()
    {
        OnGameStateChange.UnregisterAction(OnGameStateChanged);
    }
    private void LateUpdate()
    {
        TempV3 = follow.transform.position;
        TempV3.y = transform.position.y;
        transform.position = TempV3;
    }

}
    
