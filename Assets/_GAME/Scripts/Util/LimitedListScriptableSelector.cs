using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LimitedListScriptableSelector<T> : MonoBehaviour, ISelector<T> where T : class
{

    public LimitedListScriptable<T> Options;
    public LimitedListScriptable<T> SelectedOptions;

    public bool CanSelect(int index)
    {
        return !SelectedOptions.Contains(GetItem(index));
    }

    public T GetItem(int index)
    {
        return Options[index];
    }

    public bool IsAvailable(int index)
    {
        return GetItem(index) != null;
    }

    public bool IsSelected(int index)
    {
        return SelectedOptions.Contains(GetItem(index));
    }

    public void Select(int Index)
    {
        if (Options[Index] != null)
        {
            SelectedOptions.Add(Options[Index]);
        }
    }
    public void UnSelect(int Index)
    {
        if (Options[Index] != null)
        {
            SelectedOptions.Remove(Options[Index]);
        }
    }


}