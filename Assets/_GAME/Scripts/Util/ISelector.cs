using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISelector<T> where T : class
{
    public void Select(int index);
    public void UnSelect(int index);

    public bool IsSelected(int index);

    public T GetItem(int index);

    public bool IsAvailable(int index);

    public bool CanSelect(int index);


}
