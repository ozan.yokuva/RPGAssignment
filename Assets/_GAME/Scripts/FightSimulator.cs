using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class FightSimulator : MonoBehaviour
{
    Vector3 LastAttackerPosition;
    Quaternion LastAttackerRotation;
    GameObject Attacker,Opposer;
    Vector3 AttackPosition;
    ICharacterAnimationController AttackerAnimationController;
    ICharacterAnimationController OpposerAnimationController;
    ILife OpposerLifeHandler;
    Camera _MainCam;
    Camera MainCam {
        get { 
            if(_MainCam==null)
                _MainCam = Camera.main;
            return _MainCam;
        }
    }

    public GameEvent OnGameStateChangeEvent;
    public GameEvent OnAttackFinishEvent;
    public GameEvent OnAttackHitEvent;
    public GameEvent OnEnemyDeathEvent;
    public LimitedHeroListScriptable PlayerSelection;
    public LimitedHeroListScriptable OpposerSelection;

   delegate void OnComplete();
    private void Awake()
    {
        OnGameStateChangeEvent.RegisterAction(OnGameStateChanged);
        OnAttackFinishEvent.RegisterAction(OnAttackComplete);
        OnAttackHitEvent.RegisterAction(OnHit);
        OnEnemyDeathEvent.RegisterAction(OnEnemyDeath);
    }

    public void OnGameStateChanged(EventArgs Args)
    {
        switch (GameParameters.GetState())
        {

            case GameState.PlayerAttacking:
            case GameState.OpposerAttacking:
                SimulateAttack();
                break;
            case GameState.FightBegin:
                ArmFighters();
                break;
        }
    }
    public void ArmFighters()
    {

        foreach (HeroData hero in PlayerSelection)
        {
            PlayerBattleController heroController = hero.GetCurrentGameObject().AddComponent<PlayerBattleController>();
            heroController.SetHeroNContainers(hero, PlayerSelection, OpposerSelection);
            hero.GetCurrentGameObject().GetComponent<ILife>().Setup();
    
        }

        foreach (HeroData hero in OpposerSelection)
        {
            EnemyBattleController heroController = hero.GetCurrentGameObject().AddComponent<EnemyBattleController>();
            heroController.SetHeroNContainers(hero, OpposerSelection, PlayerSelection);
            heroController.OnGameStateChange = OnGameStateChangeEvent;
            hero.GetCurrentGameObject().GetComponent<ILife>().Setup();
            heroController.register();
        }

    }
    public void OnEnemyDeath(EventArgs Args)
    {
       
    
    }
    public void SimulateAttack()
    {
    
        Attacker = BattleData.Instance.Attacker.GetCurrentGameObject();
        Opposer = BattleData.Instance.Opposer.GetCurrentGameObject();
        LastAttackerPosition = Attacker.transform.position;
        LastAttackerRotation = Attacker.transform.rotation;
        AttackPosition = Vector3.MoveTowards(Opposer.transform.position, LastAttackerPosition, 
            GameParameters.Instance.float_AttackDistance);

        AttackerAnimationController = Attacker.GetComponentInChildren<ICharacterAnimationController>();
        OpposerAnimationController = Opposer.GetComponentInChildren<ICharacterAnimationController>();
        AttackerAnimationController.StartRun();
        StartCoroutine(RotateAndMove(LastAttackerPosition,AttackPosition,
            GameParameters.Instance.float_AttackRunDuration, OnFirstRunComplete));
    }
    public void OnFirstRunComplete()
    {
        AttackerAnimationController.Attack(Random.Range(0,GameParameters.s_int_RandomizeAttackAnimationsUpTo));
    }

    public void OnAttackComplete(EventArgs Args)
    {
        StartCoroutine(RotateAndMove(AttackPosition, LastAttackerPosition,
               GameParameters.Instance.float_AttackRunDuration, OnRunBackComplete));
    }
    public void OnHit(EventArgs Args)
    {
        Opposer.GetComponent<ILife>().GetDamage(BattleData.Instance.Attacker.GetAttackPower());
        FlyingText.fly("-"+BattleData.Instance.Attacker.GetAttackPower(), MainCam.WorldToScreenPoint(Opposer.transform.position + Vector3.up*2), 2, 1,.5f);
    }

    public void OnRunBackComplete()
    {
        AttackerAnimationController.StartIdle();
        Attacker.transform.rotation = LastAttackerRotation;
        switch (GameParameters.GetState())
        {
            case GameState.PlayerAttacking:
                GameParameters.SetState(GameState.OpposerSelectFighter);
                break;
            case GameState.OpposerAttacking:
                GameParameters.SetState(GameState.PlayerSelectFighter);
                break;
        }
        if (GameParameters.Instance.bool_IsGameOver)
        {
            StopAllCoroutines();
            GameParameters.SetState(PlayerSelection.Count == 0 ? GameState.Loose : GameState.Win);
        }
    }
    IEnumerator RotateAndMove(Vector3 startPos, Vector3 targetPosition, float duration, OnComplete Oncomplete)
    {
        float DurationBackup = duration;
        float DurationActive = duration;
        while (DurationActive > 0) 
        {
            Attacker.transform.right = (targetPosition - Attacker.transform.position);
            DurationActive = Mathf.Clamp(DurationActive - Time.deltaTime,0f,DurationBackup);
            Attacker.transform.position = Vector3.Lerp(startPos, targetPosition, 1f - (DurationActive / DurationBackup));
            yield return new WaitForEndOfFrame();
        }
        Oncomplete.Invoke();
    }

    private void OnDestroy()
    {
        OnGameStateChangeEvent.UnregisterAction(OnGameStateChanged);
        OnAttackFinishEvent.UnregisterAction(OnAttackComplete);
        OnAttackHitEvent.UnregisterAction(OnHit);
        OnEnemyDeathEvent.UnregisterAction(OnEnemyDeath);
    }

}
