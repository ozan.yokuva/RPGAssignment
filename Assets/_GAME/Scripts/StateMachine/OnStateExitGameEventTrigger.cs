using UnityEngine;

public class OnStateExitGameEventTrigger : StateMachineBehaviour
{
    public GameEvent OnAttackFinish;
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        OnAttackFinish?.Invoke();
    }
}
