using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicCharacterAnimationController : MonoBehaviour,ICharacterAnimationController
{
    [SerializeField] Animator animator;
    [SerializeField] string TriggerStringAttack;
    [SerializeField] string TriggerStringDeath;
    [SerializeField] string TriggerStringRun;
    [SerializeField] string TriggerStringIdle;
    [SerializeField] string TriggerStringGetHit;
    [SerializeField] string FloatStringAttackType;
    int TriggerAttack,TriggerDeath,TriggerRun,TriggerIdle,TriggerGetHit,FloatAttackType;
    public void Attack(int ID)
    {
        animator.SetFloat(FloatAttackType, ID);
        animator.SetTrigger(TriggerAttack);
    }

    public void Die()=> animator.SetTrigger(TriggerDeath);
    public void GetHit()=> animator.SetTrigger(TriggerGetHit);
    public void StartIdle()=> animator.SetTrigger(TriggerIdle);
    public void StartRun()=>animator.SetTrigger(TriggerRun);
    
    void Start()
    {
        TriggerAttack = Animator.StringToHash(TriggerStringAttack);
        TriggerIdle = Animator.StringToHash(TriggerStringIdle);
        TriggerRun  = Animator.StringToHash(TriggerStringRun);
        TriggerDeath = Animator.StringToHash(TriggerStringDeath);
        TriggerGetHit = Animator.StringToHash(TriggerStringGetHit);
        FloatAttackType = Animator.StringToHash(FloatStringAttackType);
    }

   
}
