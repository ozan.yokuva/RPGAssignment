using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBattleController : BattleController
{
    public override void OnMouseDown()
    {
        base.OnMouseDown();
    }
    public override void OnMouseExit()
    {
        base.OnMouseExit();
    }

    public override void OnMouseUp()
    {
        if(!InfoPopup.isUp())
        if (GameParameters.IsState(GameState.PlayerSelectTarget) ||
                GameParameters.IsState(GameState.PlayerSelectFighter))
            {

            BattleData.Instance.Attacker = _HeroData;
            GameParameters.SetState(GameState.PlayerSelectTarget,true);
            }
        base.OnMouseUp();
    }

}
