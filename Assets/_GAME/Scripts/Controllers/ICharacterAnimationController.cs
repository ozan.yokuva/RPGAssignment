using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICharacterAnimationController 
{
    public void StartRun();
    public void StartIdle();

    public void Attack(int ID);
    public void Die();
    public void GetHit();


}
