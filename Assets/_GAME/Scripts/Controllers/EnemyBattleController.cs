using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyBattleController : BattleController
{
    public GameEvent OnGameStateChange;

    public void register()
    {
        StartCoroutine(registerNextFrame());
        
    }

    public void OnGameStateChanged(EventArgs Args)
    {

        switch (GameParameters.GetState())
        {
            case GameState.PlayerSelectTarget:
                SelectEnemyOpposer();
                break;
            case GameState.OpposerSelectTarget:
                SelectPlayerOpposer();
                break;
            case GameState.OpposerSelectFighter:
                SelectEnemyAttacker();
                break;
        }
    }
    public void SelectEnemyOpposer()
    {
        if (_Selection.Count < 2)
        {
            BattleData.Instance.Opposer = _HeroData;
            GameParameters.SetState(GameState.PlayerAttacking);
        }
    }
    public void SelectEnemyAttacker()
    {
        BattleData.Instance.Attacker = _Selection[Random.Range(0, _Selection.Count)];
        GameParameters.SetState(GameState.OpposerSelectTarget);

    }

    public void SelectPlayerOpposer()
    {
        BattleData.Instance.Opposer = _OpposerSelection[Random.Range(0, _OpposerSelection.Count)];
        GameParameters.SetState(GameState.OpposerAttacking);
    }

    IEnumerator registerNextFrame()
    { 
    yield return new WaitForEndOfFrame();
        OnGameStateChange.RegisterAction(OnGameStateChanged);
    }

    public override void OnMouseUp()
    {
        if (!InfoPopup.isUp())
        {
            if (GameParameters.IsState(GameState.PlayerSelectTarget))
            {
                BattleData.Instance.Opposer = _HeroData;
                GameParameters.SetState(GameState.PlayerAttacking);
            }
        }
        base.OnMouseUp();
    }
    private void OnDestroy()
    {
        OnGameStateChange.UnregisterAction(OnGameStateChanged);
    }
}
