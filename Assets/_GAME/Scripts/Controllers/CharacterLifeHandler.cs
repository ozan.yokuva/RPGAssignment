using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterLifeHandler : MonoBehaviour, ILife
{
    BattleController _BattleController;
    ICharacterAnimationController _AnimationController;
    ILife.OnLifeChange _LifeChangeAction;
    float _Health;

    public GameEvent _OnCharacterDeathEvent;
    public bool _isDead {
        get
        {
            return _Health == 0;
        }
    } 
    public void Setup()
    { 
    _BattleController = GetComponent<BattleController>();
     _AnimationController = GetComponentInChildren<ICharacterAnimationController>();
     _Health = _BattleController._HeroData.GetHealth();
    }

    public void Die()
    {
        _AnimationController.Die();
        _BattleController._Selection.Remove(_BattleController._HeroData);
        Destroy(_BattleController);
        _OnCharacterDeathEvent.Invoke();
    }

    public void GetDamage(float damage)
    {
        _Health -= damage;
        
        if (_Health<=0)
            Die();
        else
          _AnimationController.GetHit();

        _LifeChangeAction?.Invoke();
    }

    public float GetHealt()
    {
        return _Health;
    }

    public float GetHealtPercent()
    {
        return (float)_Health / _BattleController._HeroData.GetHealth();
    }

    public void Heal(float healt)
    {
        _Health =Mathf.Clamp(healt + _Health, 0, _BattleController._HeroData.GetHealth());
        _LifeChangeAction?.Invoke();
    }
    public bool IsDead()
    {
    return _isDead;
    }
    public void SetHealt(float healt)
    {
        _Health = Mathf.Clamp(healt, 0, _BattleController._HeroData.GetHealth());
    }
    public void RegisterOnLifeChange(ILife.OnLifeChange lifeChanged)
    {
        _LifeChangeAction = lifeChanged;
    }
}
