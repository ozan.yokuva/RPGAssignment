using UnityEngine;

public class BattleController : MonoBehaviour
{
    public LimitedHeroListScriptable _Selection;
    public LimitedHeroListScriptable _OpposerSelection;
    public HeroData _HeroData;
    //Support with a SingletonScriptable Battle Data;
    public void SetHeroNContainers(HeroData Hero,LimitedHeroListScriptable Selection, LimitedHeroListScriptable OpposerSelection)
    { 
        _HeroData = Hero;
        this._Selection = Selection;
        this._OpposerSelection = OpposerSelection;
    }
    public void setHeroData(HeroData HeroData)
    {
        _HeroData = HeroData;
    }
    public virtual void OnMouseDown() => InfoPopup.ClickStarted(_HeroData.ToString(), 3);//Maybe Make that 3 static

    public virtual void OnMouseExit() => InfoPopup.ClickCancelled();


    public virtual void OnMouseUp()
    {
        if (InfoPopup.isUp())
            InfoPopup.stop();
        else InfoPopup.ClickCancelled();
    }
   
}
