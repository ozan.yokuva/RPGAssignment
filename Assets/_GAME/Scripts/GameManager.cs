using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public ScriptableObject[] BuildRequiredScriptables;
    private void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        GameParameters.Reset();
    }
    public void Reload()
    {
        SceneManager.LoadScene(0);
    }
}
