using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BattleData", menuName = "ScriptableObject/Data/Game/BattleData")]
public class BattleData : ScriptableObjectSingleton<BattleData>
{
    public HeroData Attacker;
    public HeroData Opposer;

}
