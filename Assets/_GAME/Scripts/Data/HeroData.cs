using UnityEngine;

[CreateAssetMenu(fileName = "New Hero", menuName = "ScriptableObject/Data/Game/HeroData")]
public class HeroData : ScriptableObject
{
    GameObject CurrentGameObject;
    [SerializeField] string string_Name="Hero";
    [SerializeField] float float_Health=100;
    [SerializeField] float float_AttackPower=100;
    [SerializeField] float float_Experience;
    [SerializeField] int int_Level=0;
    [SerializeField] float float_LevelUpDifference = 1.1f;
    [SerializeField] GameObject CharacterPrefab;
    [SerializeField] GameObject ModelPrefab;
    [SerializeField] Sprite Icon;

    #region GetSet
    public string GetName(){return string_Name;}
    public float GetExperience() { return float_Experience; }
    public GameObject GetModelPrefab(){return ModelPrefab;}
    public Sprite GetIcon() { return Icon; }
    public float GetHealth()
    {
        return float_Health * Mathf.Pow(float_LevelUpDifference, int_Level);
    }
    public float GetAttackPower()
    {
        return float_AttackPower * Mathf.Pow(float_LevelUpDifference, int_Level);
    }
    public GameObject GetCurrentGameObject()
    {
        if (CurrentGameObject == null)
            CurrentGameObject = CreateGameObject();

        return CurrentGameObject;
    }
    public int GetLevel()
    {
        return (int_Level + 1);
    }

    #endregion
    #region override

    public override string ToString()
    {
        return "Name : " + string_Name + "\n" +
            "Level : " + GetLevel() + "\n" +
            "AttackPower : " + GetAttackPower() +"\n"+
            "Experience : "+ float_Experience;
            
    }
    #endregion 
    GameObject CreateGameObject()
    {
        if(CharacterPrefab == null)
            CharacterPrefab = GameParameters.GetCharacterBasePrefab();
        GameObject TempBase = Instantiate(CharacterPrefab);
        Instantiate(ModelPrefab, TempBase.transform);
        return TempBase;
    }
    public bool AddExperience(int val)
    {
        float_Experience += val;
        if (float_Experience >= GameParameters.Instance.int_ExReq4LevelUp)
        {
            float_Experience -= GameParameters.Instance.int_ExReq4LevelUp;
            int_Level++;
            Save();
            return true;
        }
        Save();
        return false;
    }
    public void Load()
    {
        string JsonStr = PlayerPrefs.GetString("Hero_" + this.GetInstanceID(), "");
        if (JsonStr != "")
        {
            HeroSaveable newSaveable = JsonUtility.FromJson<HeroSaveable>(JsonStr);
            int_Level = newSaveable.int_Level;
            float_Experience = newSaveable.int_Experience;
        }
    }
    public void Save()
    {
        HeroSaveable newSaveable = new HeroSaveable(this);
        PlayerPrefs.SetString("Hero_"+this.GetInstanceID(), JsonUtility.ToJson(newSaveable));
    }
    public void HolyReset()
    {
        float_Experience = 0;
        int_Level = 0;
        CurrentGameObject = null;
    }
}
public class HeroSaveable { 

public int int_Level;
public float int_Experience;
    public HeroSaveable(HeroData data)
    {
        int_Level      = data.GetLevel()-1;
        int_Experience = data.GetExperience();
    }
}
