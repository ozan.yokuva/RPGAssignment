using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Parameters", menuName = "ScriptableObject/Data/Game/GameParameters")]
public class GameParameters : ScriptableObjectSingleton<GameParameters>
{
    GameState State;
    [SerializeField] GameObject CharacterBasePrefab;
    [SerializeField] GameEvent OnGameStateChangeEvent;
    [SerializeField] LimitedHeroListScriptable AllHeros;
    [SerializeField] LimitedHeroListScriptable PlayerAvailableHeros;
    [SerializeField] LimitedHeroListScriptable OpposerAvailableHeros;
    [SerializeField] LimitedHeroListScriptable PlayerSelectedHeros;
    [SerializeField] LimitedHeroListScriptable OpposerSelectedHeros;
    [SerializeField] int int_PlayerCharacterCount;
    [SerializeField] int int_OpposerCharacterCount;

    public static readonly Vector3 s_Vector3_NoWhere = Vector3.one * 9999;
    public static readonly float s_GizmoSphereRadius = .5f;
    public static readonly int s_MaxCharacterCountPerSide = 3;
    public static int s_int_RandomizeAttackAnimationsUpTo = 3;//3 attack animations so 3+ means just 3.

    public int int_ExReq4LevelUp = 5;
    public int int_BattleExReq4NewChar = 5;
    public int int_BattleExperience;
    public float float_AttackDistance = 1f;
    public float float_AttackRunDuration = 1f;

    public bool bool_IsGameOver
    {
        get
        {
            return PlayerSelectedHeros.Count == 0 || OpposerSelectedHeros.Count == 0;
        }
    }

    #region GetSet
    public static GameObject GetCharacterBasePrefab()
    {
        return Instance.CharacterBasePrefab;
    }

    public static int GetPlayerCharacterCount()
    {
        return Instance.int_PlayerCharacterCount;
    }
    public static int GetAICharacterCount()
    {
        return Instance.int_OpposerCharacterCount;
    }
    public static LimitedHeroListScriptable GetAllHeros()
    {
        return Instance.AllHeros;
    }
    public static LimitedHeroListScriptable GetPlayerSelectedHeros()
    {
        return Instance.PlayerSelectedHeros;
    }
    public static LimitedHeroListScriptable GetOpposerSelectedHeros()
    {
        return Instance.OpposerSelectedHeros;
    }

    public static LimitedHeroListScriptable GetPlayerAvailableHeros()
    {
        return Instance.PlayerAvailableHeros;
    }
    public static LimitedHeroListScriptable GetOpposerAvailableHeros()
    {
        return Instance.OpposerAvailableHeros;
    }


    public static bool IsState(GameState state)
    {
        return Instance.State == state;
    }
    public static void SetState(GameState state, bool forceEvent = false)
    {
        if (Instance.State != state || forceEvent)
        {
            Instance.State = state;
            Instance.OnGameStateChangeEvent?.Invoke();
        }
    }
    public static GameState GetState()
    {
        return Instance.State;
    }

    #endregion
    public static void Reset()
    {
        Instance.PlayerSelectedHeros.Clear();
        Instance.OpposerSelectedHeros.Clear();
        Instance.State = GameState.Selection;
        Instance.PlayerSelectedHeros.limit = Instance.int_PlayerCharacterCount;
        Instance.OpposerSelectedHeros.limit = Instance.int_OpposerCharacterCount;
        foreach (HeroData hero in Instance.AllHeros)
            hero.Load();
        Load();
    }
    public static HeroData AddBattleExperience(int val)
    {
        Instance.int_BattleExperience += val;
        if (Instance.int_BattleExperience >= Instance.int_BattleExReq4NewChar)
        {
            Instance.int_BattleExperience -= Instance.int_BattleExReq4NewChar;
            HeroData heroData = AssignNewHero();
            Save();
            return heroData;
        }
        Save();
        return null;
    }
    public static HeroData AssignNewHero()
    {
        List<HeroData> available = GetAllHeros().TheList.FindAll(
            newHero => !GetPlayerAvailableHeros().Contains(newHero)
            && !GetOpposerAvailableHeros().Contains(newHero));
        if (available.Count > 0)
        {
            HeroData newHero = available[Random.Range(0, available.Count)];
            GetPlayerAvailableHeros().Add(newHero);
            return newHero;
        }
        else
            return null;
    }
    public void HolyReset()
    {
        PlayerPrefs.DeleteAll();

        foreach (HeroData hero in AllHeros)
        {
            hero.HolyReset();

        }
        while (PlayerAvailableHeros.Count > 3)
            PlayerAvailableHeros.RemoveAt(3);
        int_BattleExperience = 0;

    }
    public static void Load()
    {
        string JsonStr = PlayerPrefs.GetString("GameParameters", "");
        if (JsonStr != "")
        {
            ParametersSaveable newSaveable = JsonUtility.FromJson<ParametersSaveable>(JsonStr);
            Instance.int_BattleExperience = newSaveable.int_BattleExperience;
            Instance.int_PlayerCharacterCount = newSaveable.int_PlayerCount;
            Instance.int_OpposerCharacterCount = newSaveable.int_OpposerCount;
            for (int i = 0; i < newSaveable.A_int_AvailableHeros.Length; i++)
            {
                if (!Instance.PlayerAvailableHeros.Contains(Instance.AllHeros[newSaveable.A_int_AvailableHeros[i]]))
                    Instance.PlayerAvailableHeros.Add(Instance.AllHeros[newSaveable.A_int_AvailableHeros[i]]);
            }
        }
    }
    public void RotateEnemy()
    {
        int_OpposerCharacterCount++;
        int maxChar = s_MaxCharacterCountPerSide + 1;
        int_OpposerCharacterCount = int_OpposerCharacterCount % maxChar == 0?1:
            int_OpposerCharacterCount % maxChar;
        Save();
    }

    public void RotatePlayer()
    {
        int maxChar = s_MaxCharacterCountPerSide + 1;
        int_PlayerCharacterCount++;
        int_PlayerCharacterCount = int_PlayerCharacterCount % maxChar == 0 ? 1 :
            int_PlayerCharacterCount % maxChar;
        Save();
    }

    public static void Save()
    {
        ParametersSaveable newSaveable = new ParametersSaveable(Instance);
        PlayerPrefs.SetString("GameParameters", JsonUtility.ToJson(newSaveable));
    }
}
public class ParametersSaveable
{
    public int[] A_int_AvailableHeros;
    public int int_BattleExperience;
    public int int_PlayerCount;
    public int int_OpposerCount;
    public ParametersSaveable(GameParameters data)
    {
        int_PlayerCount = GameParameters.GetPlayerCharacterCount();
        int_OpposerCount = GameParameters.GetAICharacterCount();
        int_BattleExperience = data.int_BattleExperience;
        A_int_AvailableHeros = new int[GameParameters.GetPlayerAvailableHeros().Count];
        for (int i = 0; i < A_int_AvailableHeros.Length; i++)
        {
            A_int_AvailableHeros[i] = GameParameters.GetAllHeros().
                IndexOf(GameParameters.GetPlayerAvailableHeros()[i]);
        }
    }
}
public enum GameState
{
    Selection,
    FightBegin,
    PlayerSelectFighter,
    PlayerSelectTarget,
    PlayerAttacking,
    OpposerSelectFighter,
    OpposerSelectTarget,
    OpposerAttacking,
    Win,
    Loose
}
