using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LimitedListScriptable<T> : ListScriptable<T> where T : class
{
    public int limit =1;
    public bool isFull
    {
        get
        {
            if(TheList ==null)
                TheList = new List<T>();
            return TheList.Count == limit;
        }
    }

  
    public override void Add(T Candidate)
    {
        if (!isFull)
        {
            base.Add(Candidate);
            OnElementAdd(Candidate);
            if (isFull)
                OnFillStateChanged(Candidate);  
          
        }

       
    }

    public override bool Remove(T Candidate)
    {
        if (TheList != null && Contains(Candidate))
        { 
            bool baseRemoveResult = base.Remove(Candidate);
            OnElementRemoved(Candidate);
            OnFillStateChanged(Candidate);
            return baseRemoveResult;
        }

        return false;
    }
    
   

    public abstract void OnElementAdd(T NewElement);
    public abstract void OnElementRemoved(T ElementRemoved);
    public abstract void OnFillStateChanged(T LastActionElement);

    
}
