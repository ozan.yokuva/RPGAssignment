using UnityEngine;
using UnityEditor;


public abstract class ScriptableObjectSingleton<T> : ScriptableObject where T : ScriptableObjectSingleton<T>
{
// Author : Ozan Yokuva 04/02/2022
// Version 1.01.03 - CreateForMe Option is eleminated, for
// Extender is now supposed to decide how to create Instances
// Version 1.01.02 - Editor tag fix
// This is the Abstract Class of ScriptableObjectSingleton. 
// It is used to convert our ScriptableObject into a Singleton
// It is STRONGLY adviced to reference them
// atleast in 1 scene, to get Unity put them in the build archive
// Otherwise Your scriptables will be ignored by builder. 

    static protected T _Instance;
    static public T Instance
    {
        get
        {
            if (_Instance == null)
            {

                T[] CurrentInstances = Resources.FindObjectsOfTypeAll<T>();

                if (CurrentInstances.Length == 0)
                {
                    throw new MissingReferenceException("No valid Instances of (" + typeof(T) + ")");
                }
                else if (CurrentInstances.Length > 1)
                {
                    string warning = "There are more than 1(" + CurrentInstances.Length + ") instances of " + typeof(T) + "...";
                    #if UNITY_EDITOR
                    for (int i = 0; i < CurrentInstances.Length; i++)
                    {
                        warning += "\\n" + (i + 1) + ":" + AssetDatabase.GetAssetPath(CurrentInstances[i]);

                    }
                    Debug.LogWarning("ActiveInstance is :(" + AssetDatabase.GetAssetPath(CurrentInstances[0]) + ") ");
                    #endif
                    Debug.LogWarning(warning);
                    Debug.LogWarning(warning);
                }
                else
                {
                    _Instance = CurrentInstances[0];
                }

            }
            return _Instance;
        }
    }

}
