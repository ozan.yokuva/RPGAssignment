using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListScriptable<T> : ScriptableObject,IList<T> where T : class
{
    public List<T> TheList;

    #region IList Implementation
    public T this[int index] { get =>index<TheList.Count?TheList[index]:null; set { if (index < TheList.Count) TheList[index] = value; } }

    public int Count => TheList.Count;

    public bool IsReadOnly => false;

    public virtual void Add(T item)
    {
        
        TheList.Add(item);
    }
    

    public void Clear()
    {
        TheList.Clear();
    }

    public bool Contains(T item)
    {
       return TheList.Contains(item);
    }

    public void CopyTo(T[] array, int arrayIndex)
    {
        TheList.CopyTo(array, arrayIndex);
    }

    public IEnumerator<T> GetEnumerator()
    {
        return TheList.GetEnumerator();
    }

    public int IndexOf(T item)
    {
        return TheList.IndexOf(item);
    }

    public void Insert(int index, T item)
    {
       TheList.Insert(index, item);
    }

    virtual public bool Remove(T item)
    {
        return TheList.Remove(item);
    }

    public void RemoveAt(int index)
    {
       TheList.RemoveAt(index);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return TheList.GetEnumerator();
    }
    #endregion
}
