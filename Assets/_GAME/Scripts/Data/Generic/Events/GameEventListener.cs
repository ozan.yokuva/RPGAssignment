using UnityEngine;
using System;
using UnityEngine.Events;

public class GameEventListener : MonoBehaviour, IGameEventListener
    {
        [SerializeField] GameEvent _GameEvent;
        [SerializeField] UnityEvent _GameEventTrigger;
        public void RaiseEvent(EventArgs args = null)
        {
                _GameEventTrigger?.Invoke();
        }
        public void Awake() => _GameEvent.Register(this);
        public void OnDestroy() => _GameEvent.Unregister(this);
    }
