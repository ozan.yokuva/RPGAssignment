using System;

public interface IGameEventListener 
{
    public void RaiseEvent(EventArgs Args);
}
