using System;
public interface  IGameEventTrigger 
    {        public void RaiseEvent(EventArgs eventArgs);
    }
