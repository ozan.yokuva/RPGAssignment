using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "LimitedHeroListScriptable", menuName = "ScriptableObject/Data/Game/LimitedHeroListScriptable")]
public class LimitedHeroListScriptable : LimitedListScriptable<HeroData>
{
    public GameEvent OnAdd;
    public GameEvent OnRemove;
    public GameEvent OnFillStateChange;
    public override void OnElementAdd(HeroData newElement)
    {
        OnAdd?.Invoke(new GenericEventArg<HeroData>(newElement, GenericEventType.OnContainerAdd));
    }
    public override void OnElementRemoved(HeroData elementRemoved)
    {
        OnRemove?.Invoke(new GenericEventArg<HeroData>(elementRemoved, GenericEventType.OnContainerRemove));
    }
    public override void OnFillStateChanged(HeroData elementRemoved)
    {
        OnFillStateChange?.Invoke(new GenericEventArg<HeroData>(elementRemoved, GenericEventType.OnContainerFillStateChanged));
    }
}
